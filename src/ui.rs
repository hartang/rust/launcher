/// Handle UI for the application
use crate::actions::Action;
use anyhow::{Context, Result};
use crossterm::{
    self,
    event::{self, Event, KeyCode, KeyEvent, KeyModifiers},
    terminal::{self, disable_raw_mode, enable_raw_mode},
};
use tui::{
    backend::CrosstermBackend,
    style::{Color, Modifier, Style},
    text::{Span, Text},
    widgets::{self, Block, BorderType, Borders},
    Terminal,
};

use std::{
    fmt,
    io::Read,
    process::{self, ExitStatus},
    sync::mpsc,
    time,
};
use std::{io, thread, time::Duration};

type Backend = CrosstermBackend<io::Stdout>;
// TODO(hartan): Use `wezterm::input::InputEvent` at some later point in time here
// Or maybe `crossterm::event::read()`
type Message = KeyEvent;
type AnyError = anyhow::Error;

/// Possible states of an action
#[derive(Debug)]
enum ActionState {
    /// Action hasn't been run yet
    Idle,
    /// Action is currently executing
    Executing(process::Child),
    /// Actions finished executing with given exit code
    Finished(ExitStatus),
    /// OS-level error occured while executing action
    Error(AnyError),
}

/// Handle to an action
#[derive(Debug)]
struct ActionHandle {
    /// Associated action to execute
    action: Action,
    /// Current state of the action
    state: ActionState,
    /// Contents of stdout
    stdout: String,
    /// Contents of stderr
    stderr: String,
    /// Time when last execution was started
    start: time::Instant,
}

impl ActionHandle {
    pub fn new(action: Action) -> Self {
        Self {
            action,
            state: ActionState::Idle,
            stdout: String::new(),
            stderr: String::new(),
            start: time::Instant::now(),
        }
    }
}

/// Conversion
impl From<Action> for ActionHandle {
    fn from(value: Action) -> Self {
        Self::new(value)
    }
}

impl fmt::Display for ActionHandle {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let key = self.action.key.to_string();
        let state = match self.state {
            ActionState::Idle => "...".to_string(),
            ActionState::Finished(num) => format!("{:3}", num),
            ActionState::Error(_) => " X ".to_string(),
            ActionState::Executing(_) => match self.start.elapsed().as_millis() % 1000 {
                0..=249 => " - ",
                250..=499 => " / ",
                500..=749 => " | ",
                750..=999 => " \\ ",
                _ => unreachable!(),
            }
            .to_string(),
        };
        let text = self.action.action.join(" ");
        writeln!(f, " {:>6}  {:3}  {}", key, state, text)
    }
}

#[allow(clippy::from_over_into)]
impl<'a> Into<Text<'a>> for &ActionHandle {
    fn into(self) -> Text<'a> {
        //let mut text = Text::from(self.action.key.to_string());
        let key = self.action.key.to_string();

        let state = match self.state {
            ActionState::Idle => Span::raw("   "),
            ActionState::Finished(state) => match state.code() {
                Some(code) => {
                    let style =
                        Style::default().fg(if code == 0 { Color::Green } else { Color::Red });
                    Span::styled(format!("{:3}", code), style)
                }
                None => Span::styled("SIG", Style::default().fg(Color::Red)),
            },
            ActionState::Error(_) => Span::styled(" X ", Style::default().fg(Color::Red)),
            ActionState::Executing(_) => Span::styled(
                match self.start.elapsed().as_millis() % 1000 {
                    0..=249 => " - ",
                    250..=499 => " / ",
                    500..=749 => " | ",
                    750..=999 => " \\ ",
                    _ => unreachable!(),
                }
                .to_string(),
                Style::default().fg(Color::Yellow),
            ),
        };

        let description = self.action.action.join(" ");

        let spans = tui::text::Spans::from(vec![
            Span::styled(key, Style::default().fg(Color::Blue)),
            Span::raw("  "),
            state,
            Span::raw("  "),
            Span::raw(description),
        ]);

        Text::from(spans)
    }
}

pub fn main(actions: Vec<Action>) -> Result<()> {
    let err_context = "failed to execute UI";

    let mut terminal = setup_terminal().context(err_context)?;
    let (tx, rx) = mpsc::channel::<Message>();

    let _handle = thread::Builder::new()
        .name("stdin-handler".to_string())
        .spawn(move || input_handler(tx))
        .context(err_context)?;

    let mut list_state = widgets::ListState::default();
    list_state.select(Some(0));

    let mut action_list = actions
        .iter()
        .map(|action| ActionHandle::from(action.clone()))
        .collect::<Vec<_>>();

    loop {
        let key = match rx.try_recv() {
            Ok(key) => key,
            Err(mpsc::TryRecvError::Empty) => {
                // Stop it from eating up all CPU time
                thread::sleep(Duration::from_millis(50));
                KeyEvent::new(KeyCode::Char('l'), KeyModifiers::CONTROL)
            }
            Err(e) => return Err::<(), _>(anyhow::Error::new(e)).context(err_context),
        };

        match key.code {
            KeyCode::Char('q') => break,
            KeyCode::Char('j') | KeyCode::Down => {
                list_state.select(
                    list_state
                        .selected()
                        .map(|idx| idx.saturating_add(1))
                        .or(Some(0)),
                );
            }
            KeyCode::Char('k') | KeyCode::Up => {
                list_state.select(
                    list_state
                        .selected()
                        .map(|idx| idx.saturating_sub(1))
                        .or(Some(0)),
                );
            }
            KeyCode::Enter => {
                let cur_action = list_state
                    .selected()
                    .context("no list entry was selected")
                    .and_then(|id| {
                        action_list
                            .get_mut(id)
                            .context("illegal list entry was selected")
                    })
                    .context(err_context)?;
                let mut cmdline = cur_action.action.action.iter();
                match std::process::Command::new(cmdline.next().unwrap())
                    .args(cmdline)
                    .spawn()
                    .context(err_context)
                {
                    Ok(handle) => cur_action.state = ActionState::Executing(handle),
                    Err(err) => {
                        log::error!("{}", err);
                        cur_action.state = ActionState::Error(err);
                    }
                }
            }
            _ => (),
        }

        // Iterate through actions, check their exit status
        action_list.iter_mut().for_each(|action| {
            if let ActionState::Executing(handle) = &mut action.state {
                let _ = match handle.try_wait().context(err_context) {
                    Ok(Some(status)) => {
                        if let Some(stdout) = &mut handle.stdout {
                            // TODO(hartan): Clean this up
                            let _ = stdout.read_to_string(&mut action.stdout);
                        };
                        if let Some(stderr) = &mut handle.stderr {
                            // TODO(hartan): Clean this up
                            let _ = stderr.read_to_string(&mut action.stderr);
                        };
                        Some(ActionState::Finished(status))
                    }
                    Ok(None) => None,
                    Err(err) => {
                        log::error!("{}", err);
                        Some(ActionState::Error(err))
                    }
                }
                .map(|state| {
                    action.state = state;
                });
            }
        });

        terminal
            .draw(|f| {
                let size = f.size();
                let action_list = widgets::List::new(
                    action_list
                        .iter()
                        .map(widgets::ListItem::new)
                        .collect::<Vec<_>>(),
                )
                .block(
                    Block::default()
                        .title(" Actions ")
                        .borders(Borders::ALL)
                        .border_type(BorderType::Rounded),
                )
                .style(Style::default().fg(Color::Green))
                .highlight_style(Style::default().add_modifier(Modifier::ITALIC))
                .highlight_symbol(">> ");

                f.render_stateful_widget(action_list, size, &mut list_state);
            })
            .context(err_context)?;
        thread::sleep(Duration::from_millis(100));
    }

    restore_terminal(&mut terminal).context(err_context)
}

// Read input and send it through the given channel
fn input_handler(channel: mpsc::Sender<Message>) -> Result<()> {
    let err_context = "failed to read process input";

    loop {
        event::read()
            .map_err(anyhow::Error::new)
            .and_then(|event| match event {
                Event::Key(event) => channel.send(event).map_err(AnyError::new),
                Event::Resize(_, _) => channel
                    .send(KeyEvent::new(KeyCode::Char('l'), KeyModifiers::CONTROL))
                    .map_err(AnyError::new),
                _ => Ok(()),
            })
            .context(err_context)?;
    }
}

fn setup_terminal() -> Result<Terminal<Backend>> {
    let mut stdout = io::stdout();
    enable_raw_mode()
        .and_then(|_| crossterm::execute!(stdout, terminal::EnterAlternateScreen))
        .and_then(|_| Terminal::new(CrosstermBackend::new(stdout)))
        .context("failed to setup terminal for application")
}

fn restore_terminal(terminal: &mut Terminal<Backend>) -> Result<()> {
    disable_raw_mode()
        .and_then(|_| crossterm::execute!(terminal.backend_mut(), terminal::LeaveAlternateScreen))
        .and_then(|_| terminal.show_cursor())
        .context("failed to restore terminal to previous state")
}
