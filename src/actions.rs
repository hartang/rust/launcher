/// Basic definitions for Actions
///
/// This module primarily handles parsing the available actions from a config file. The config is
/// written in YAML and looks, for example, like this:
///
/// ```yaml
/// - key: 'a'
///   action: ["bash", "-ic", "echo", "Hello there"]
///   description: "Say hello"
///   id: 1
/// ```
use anyhow::{Context, Result};
use serde::{Deserialize, Serialize};
use std::fmt;
use std::path::PathBuf;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Action {
    pub key: char,
    pub action: Vec<String>,
    pub description: Option<String>,
    pub id: usize,
    pub depends: Option<Vec<usize>>,
}

impl fmt::Display for Action {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let action = self.action.join(" ");
        let text = self.description.as_ref().unwrap_or(&action);
        writeln!(f, "{}) {} - {}", self.id, self.key, text)
    }
}

pub fn retrieve(config: PathBuf) -> Result<Vec<Action>> {
    let err_context = || {
        format!(
            "failed to retrieve actions from config '{}'",
            config.display()
        )
    };

    let actions: Vec<Action> = std::fs::read_to_string(&config)
        .map_err(anyhow::Error::new)
        .and_then(|string| serde_yaml::from_str(&string).map_err(anyhow::Error::new))
        .with_context(err_context)?;

    // Check IDs for sanity
    let mut ids = actions.iter().map(|action| action.id).collect::<Vec<_>>();
    ids.sort();
    let mut last_id = None;
    for id in ids {
        if let Some(val) = last_id {
            if val == id {
                return Err(anyhow::anyhow!(
                    "duplicate action ID '{}' is not allowed",
                    id
                ))
                .with_context(err_context);
            }
        }
        last_id = Some(id);
    }

    // Check for duplicate keybindings
    let mut keys = actions.iter().map(|action| action.key).collect::<Vec<_>>();
    keys.sort();
    let mut last_key = None;
    for key in keys {
        if let Some(val) = last_key {
            if val == key {
                return Err(anyhow::anyhow!(
                    "duplicate keybinding '{}' is not allowed",
                    key
                ))
                .with_context(err_context);
            }
        }
        last_key = Some(key);
    }

    Ok(actions)
}
