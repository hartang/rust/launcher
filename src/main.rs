/// How does this work?
///
/// - Read configuration from a YAML config (.launcher.yaml)
///     - Checks CWD first, then recursively walks up the filesystem until one is found
///     - The config contains:
///         - A key to cause an action
///         - An action (command and arguments)
///         - Optional: Textual description of the action
///         - An ID (used mostly for sorting, or dependencies)
///         - Optional: Array of dependencies (other actions this one relies on)
/// - Presents a UI with:
///     - The keys to press,
///     - The action description or, if unavailable, the action itself
///     - The last exit status of the action
///     - Maybe the output of the action, too??
mod actions;
mod ui;

use anyhow::{Context, Result};
use std::path::PathBuf;

fn main() -> Result<()> {
    let err_context = "failed to launch actions";
    pretty_env_logger::init();

    // Try and find a config
    let config_file = find_config().context(err_context)?;
    log::info!("Using config {}", config_file.display());

    // Serialize config to our datatype
    let actions = actions::retrieve(config_file).context(err_context)?;
    log::info!("Found actions: {:?}", actions);

    // Paint UI and handle input
    ui::main(actions).context(err_context)?;
    Ok(())
}

fn find_config() -> Result<PathBuf> {
    let err_context = "failed to find config file";

    let cur_path = std::env::current_dir().context(err_context)?;
    log::debug!("Starting in directory {}", cur_path.display());
    let files = cur_path
        .ancestors()
        .flat_map(|path| {
            // Read directory entries
            if let Ok(dir_iter) = path.read_dir() {
                dir_iter
                    .filter_map(|entry| {
                        // Check if entry matches out criteria
                        if let Ok(entry) = entry {
                            if entry.file_name() == *".launcher.yaml" {
                                return Some(entry.path());
                            }
                        }
                        None
                    })
                    .collect::<Vec<_>>()
            } else {
                log::warn!("Cannot read entries in {}, ignoring", path.display());
                vec![]
            }
        })
        .collect::<Vec<_>>();

    log::info!("Configuration files found: {files:?}");
    files
        .first()
        .ok_or_else(|| anyhow::Error::msg("No valid configuration file found"))
        .map(|file| file.to_owned())
        .context(err_context)
}
